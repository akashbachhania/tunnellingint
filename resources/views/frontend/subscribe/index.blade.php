@extends('layout.frontend.master')
@section('content')
	  <section class="subpage-banner padding-bottom text-center">
	  	<div class="container">
			<h1>{!! nl2br($subscribe->title)!!}</h1>
			<hr>
			<p>{!! nl2br($subscribe->text)!!}</p>
		</div>
	  </section>
	  <section class="subscription-included">
	  	<div class="container">
		   <div class="subscription-form">
		  	<form method="post" action="{{url('/subscribe/store')}}">
		  		{{ csrf_field() }}
						<div class="form-row">
							<div class="sub-text">
								@if ($errors->has('email'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('email') }}</strong>
			                            </span>
			                    @endif	
								<div class="text-outer">
								<input type="email" class="text-filed" placeholder="" value="{{Input::old('email')}}" name="email">
								
									<span>Email</span>
								</div>
								</div>
														
						</div>
						<div class="form-row">
							<div class="sub-text">
								@if ($errors->has('name'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('name') }}</strong>
			                            </span>
			                    @endif	
								<div class="text-outer">
								<input type="text" class="text-filed" placeholder="" value="{{Input::old('name')}}" name="name">
								
									<span>Name (optional)</span>
								</div>
								</div>
														
						</div>
						<div class="form-row">
							<div class="sub-text">
								@if ($errors->has('company'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('company') }}</strong>
			                            </span>
			                    @endif	
								<div class="text-outer">
								<input type="text" class="text-filed" placeholder="" value="{{Input::old('company')}}" name="company">
							
									<span>Company (optional)</span>
								</div>
								</div>
														
						</div>
						<div class="form-row">
							<div class="sub-text">
								@if ($errors->has('job_title'))
		                            <span class="help-block">
		                                <strong>{{ $errors->first('job_title') }}</strong>
		                            </span>
		                        @endif
								<div class="text-outer">
									<input class="text-filed" placeholder="" value="{{Input::old('job_title')}}" type="text" name="job_title">
									<span>Job title </span>
								</div>
							</div>
														
						</div>
						<div class="form-row">
							<div class="sub-text">
								@if ($errors->has('country'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('country') }}</strong>
			                            </span>
			                    @endif	
								<div class="text-outer">
									<input class="text-filed" placeholder="" value="{{Input::old('country')}}" type="text" name="country">
									<span>Country </span>
								</div>
							</div>
														
						</div>
						
						
						<div class="form-row">
							<div class="sub-text">
								<p><img src="{{url('/images/lock-gray.png')}}" alt="lock"> No SPAM Guarantee</p>
							</div>
						</div>
						<div class="form-row">
							<div class="sub-text">
								<div class="sub-btn">
									<button type="submit" class="btn-icon new-submit-btn shadow-none"><span class="btn-download">subscribe </span><span class="download-icon"><img src="{{url('/images/subscribe.png')}}" alt="subscribe"></span></button>
								</div>
							</div>
						</div>
					</form>
			</div>
			<div class="subscription-included text-center">
				<h3>INCLUDED IN YOUR SUBSCRIPTION</h3>
				<div class="subscription-row">
					<div class="col-4">
						<div class="subscription-block">
							<div class="icon"><img src="{{url('/images/up-do-date.png')}}"></div>
							<h5>Up to date tunnelling<br>industry announcements</h5>
						</div>
					</div>
					<div class="col-4">
						<div class="subscription-block">
							<div class="icon"><img src="{{url('/images/pdf-version.png')}}"></div>
							<h5>Your yearly PDF version of<br>Tunnelling International</h5>
						</div>
					</div>
					<div class="col-4">
						<div class="subscription-block">
							<div class="icon"><img src="{{url('/images/international-newsletter.png')}}"></div>
							<h5>Access to Tunnelling<br>International newsletters</h5>
						</div>
					</div>
				</div>
			</div>
		 </div>
	  </section>


 	

@endsection

