<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    @php 
      $Seotags=Helper::SeoCommontags()
    @endphp
    <title>{{$Seotags->title}}</title>
    <meta name="description" content="{{$Seotags->description}}">
    <meta name="keywords" content="{{$Seotags->keywords}}">
      <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,900" rel="stylesheet"> 
    <!-- css -->
    <!-- <link href="css/style.css" rel="stylesheet"> -->
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/fonts.css') !!}" rel="stylesheet" type="text/css" />

    


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#000"
    },
    "button": {
      "background": "transparent",
      "text": "#f1d600",
      "border": "#f1d600"
    }
  },
  "content": {
    "href": "https://tunnellingint.com/privacy"
  }
})});
</script>
  </head>
  <body>	  
    @php 
      $companyInfo=Helper::companyinfo()
    @endphp
   
	  @include('layout.frontend.header')
	  
	  @yield('content')	  	
	  @include('layout.frontend.footer')
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-105315021-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-105315021-1');
</script>
  </body>
</html>