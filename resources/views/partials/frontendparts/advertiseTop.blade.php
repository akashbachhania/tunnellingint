 @php 
    $companyInfo	=	Helper::companyinfo()
@endphp
<div class="top-section">
	<h3>for reservations contact us or fill in the form below:</h3>
	<div class="right-contact">
		<div class="topContact">
			<span><img src = "{{url('/images/message-icons.png')}}"></span><a href="mailto:{{$companyInfo->company_email}}">{{$companyInfo->company_email}}</a>
		</div>
		<div class="topContact">
			<span><img src="{{url('/images/phone-address-icon.png')}}"></span><a href="tel:{{$companyInfo->contact_no}}">{{$companyInfo->contact_no}}</a>
		</div>
	</div>
</div>