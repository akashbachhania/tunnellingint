<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaPackCount extends Model
{
    //
    public function geoDetails(){
    	return $this->hasOne('App\TrackerGeoip','id','geo_id');
    }
}
