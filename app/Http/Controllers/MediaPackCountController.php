<?php

namespace App\Http\Controllers;

use App\MediaPackCount;
use Illuminate\Http\Request;
use Tracker;
class MediaPackCountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $visitor                    =       Tracker::currentSession();
        
        $mediaPackCount             =       New MediaPackCount;

        $mediaPackCount->client_ip  =       $visitor->client_ip;

        $mediaPackCount->geo_id     =       $visitor->geoIp->id;

        $mediaPackCount->save();  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MediaPackCount  $mediaPackCount
     * @return \Illuminate\Http\Response
     */
    public function show(MediaPackCount $mediaPackCount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MediaPackCount  $mediaPackCount
     * @return \Illuminate\Http\Response
     */
    public function edit(MediaPackCount $mediaPackCount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MediaPackCount  $mediaPackCount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MediaPackCount $mediaPackCount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MediaPackCount  $mediaPackCount
     * @return \Illuminate\Http\Response
     */
    public function destroy(MediaPackCount $mediaPackCount)
    {
        //
    }
}
