<?php

namespace App\Http\Controllers\Admin;

use App\MediaPackCount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedaiPackController extends Controller
{
    //
    //
    public function index(){
    	
    	return view('admin.mediapack.index'); 
    }

    public function ajax(){
    	$mediaPacks			= 	MediaPackCount::latest('updated_at')->get();
    	$records            =   array();
        $i                  =   0;
        
        foreach ($mediaPacks as $key => $value) {

         	$records[$i]['client_ip'] 	  =    $value->client_ip;
         
         	$records[$i]['country'] 	  =	   $value->geoDetails->country_name;
         
         	$records[$i]['city'] 		  =	   $value->geoDetails->city;
        
          	$records[$i]['date']          =    $value->created_at->format('d-m-Y');

          	$i++;

        }
        
        echo json_encode($records);
    	 
    }
}
