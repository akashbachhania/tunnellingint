<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.event.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.event.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            $rand=rand();
            
            $imageName = $rand.'.'.$request->file('photo')->getClientOriginalExtension();
       
            if($request->file('photo')->move(public_path('uploads/event'), $imageName)){
                $event              =   new Event;
                $event->link        =   $request->link;
                $event->image       =   $imageName;
                $event->type        =   $request->type;
                $event->save();
                return redirect('admin/event');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //

        return view('admin.event.edit',compact("event")); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
            $rand=rand();
           
            
            $event->link        =   $request->link;
       
            
            if($request->file('photo')!=''){
                $imageName = $rand.'.'.$request->file('photo')->getClientOriginalExtension();
                
                if($request->file('photo')->move(public_path('uploads/event'), $imageName)){
                    $path = public_path("uploads/event/$event->image");
                    unlink($path);
                    $event->image       =   $imageName;
                }
            }

            $event->save();
            return redirect('admin/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
        $path = public_path("uploads/event/$event->image");
        unlink($path);
        $event->delete();
    }
    public function ajax()
    {
        //
        $events             =   Event::all();
        $records            =   array();
        $i                  =   0;
        
        foreach ($events as $key => $value) {
          //print_r(expression)
          # code...
            $editBtn                =   "<a href='".url("admin/event/$value->id/edit")."' class='btn btn-info'><i class='fa fa-pencil'></i></a> ";
            $delteBtn               =   "<a href='".route("event.destroy",['id'=>$value->id])."' data-method='delete' class='btn btn-danger delete_event' value='".$value->id."'><i class='fa  fa-trash'></i></a>";

            $records[$i]['image']   =   "<img src='".asset("uploads/event/$value->image")."' class='img img-md' />";
            $records[$i]['link']    =   $value->link;  
            $records[$i]['action']  =   $editBtn;
            $i++;
        }

        echo json_encode($records);
        die();
    }

}
